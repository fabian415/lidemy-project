let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

function solve (lines) {
    let tmp = lines[0].split(' ')
    console.log(Number(tmp[0]) + Number(tmp[1]))
}
