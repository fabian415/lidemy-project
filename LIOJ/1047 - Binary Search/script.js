let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// https://oj.lidemy.com/problem/1047
function solve (lines) {
    let tmp = lines[0].split(' ')
    let n1 = Number(tmp[0])
    let n2 = Number(tmp[1])
    let arr = []
    for (let i = 0; i < n1; i++) {
        arr.push(Number(lines[1 + i]))
    }
    for (let i = 0; i < n2; i++) {
        let number = Number(lines[1 + n1 + i])
        let index = binarySearch(arr, number)
        console.log(index)
    }
}

// function binarySearch (arr, number) {
//     let left = 0, right = arr.length - 1
//     let index = -1
//     while (left <= right) {
//         let middle = Math.floor((right + left) / 2)
//         if (arr[middle] === number) {
//             index = middle
//             break
//         } else if (arr[middle] > number) {
//             right = middle - 1
//         } else {
//             left = middle + 1
//         }
//     }
//     return index
// }

// [first, last) 左閉右開，條件可接受升冪、數字可重複
function binarySearch (arr, number) {
    let first = 0,
        last = arr.length - 1
    while (first < last) {
        let middle = Math.floor((first + last) / 2)
        if (arr[middle] < number) {
            first = middle + 1
        } else {
            last = middle
        }
    }

    if (arr[first] === number)
        return first
    else
        return -1
}

