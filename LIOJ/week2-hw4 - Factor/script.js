let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// hw4：印出因數
// 先幫大家複習一下數學，給定一個數字 n，因數就是所有小於等於 n 又可以被 n 整除的數，所以最明顯的例子就是 1 跟 n，這兩個數一定是 n 的因數。現在請寫出一個函式來「印出」所有的因數
function solve (lines) {
    let tmp = lines[0].split(' ')
    let n1 = Number(tmp[0])
    printFactor(n1)
}

function printFactor (n) {
    for (let i = 1; i < n + 1; i++) {
        if (i === 1 || i === n) {
            console.log(i)
        } else {
            if (isInteger(n / i)) {
                console.log(i)
            }
        }
    }
}

function isInteger (numb) {
    return numb % 1 === 0
}
