let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// https://oj.lidemy.com/problem/1008
function solve (lines) {
    let tmp = lines[0].split(' ')
    let n1 = Number(tmp[0])

    let result = 1
    let n = n1
    let digit = Math.log2(n)
    while (!isInteger(digit)) {
        result++
        n = n - Math.pow(2, Math.floor(digit))
        digit = Math.log2(n)
    }
    console.log(result)
}

function isInteger (numb) {
    return numb % 1 === 0
}
