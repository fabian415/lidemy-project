let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

function solve (lines) {
    let count = Number(lines[0])
    for (let i = 0; i < count; i++) {
        let [num1, num2, ans] = lines[i + 1].split(' ')
        ans = Number(ans)
        let result = compete(num1, num2, ans)
        console.log(result)
    }
}

// function compete (num1, num2, ans) {
//     if (num1.length === num2.length) {
//         if (num1 === num2) {
//             return 'DRAW'
//         } else {
//             // 流程(1) ans > 0 大的勝，num1 > num2，則回傳 'A'，反之則回傳 'B'
//             // 流程(2) ans < 0 小的勝，num1 > num2，則回傳 'B'，反之則回傳 'A'
//             // 所以如果 ans < 0，可以將num1與num2互相交換，接著走流程(1)即可。
//             if (ans < 0) {
//                 let arr = swap(num1, num2)
//                 num1 = arr[0]
//                 num2 = arr[1]
//             }
//             return num1 > num2 ? 'A' : 'B'
//         }
//     } else {
//         let lenNum1 = num1.length
//         let lenNum2 = num2.length
//         if (ans < 0) {
//             let arr = swap(lenNum1, lenNum2)
//             lenNum1 = arr[0]
//             lenNum2 = arr[1]
//         }
//         return lenNum1 > lenNum2 ? 'A' : 'B'
//     }
// }

// function swap (a, b) {
//     let tmp = a
//     a = b
//     b = tmp
//     return [a, b]
// }


// 利用 XOR 交換率的特性
// Z = a ^ b
// a = Z ^ a = (a ^ b) ^ a = (a ^ a) ^ b = 0 ^ b = b
// b = Z ^ b = (a ^ b) ^ b = a ^ (b ^ b) = a ^ 0 = a
// function swap (a, b) {
//     a = a ^ b
//     b = a ^ b
//     a = a ^ b
//     return [a, b]
// }


function compete (num1, num2, ans) {
    if (num1.length === num2.length) {
        if (num1 === num2) {
            return 'DRAW'
        }
        // TRUE ^ FALSE => A
        // TRUE ^ TRUE => B
        // FALSE ^ TRUE => A 
        // FALSE ^ FALSE => B
        return (num1 > num2) ^ (ans < 0) ? 'A' : 'B'
    } else {
        let lenNum1 = num1.length
        let lenNum2 = num2.length
        return (lenNum1 > lenNum2) ^ (ans < 0) ? 'A' : 'B'
    }
}
