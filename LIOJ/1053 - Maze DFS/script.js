let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// 題目：https://oj.lidemy.com/problem/1053
function solve (lines) {
    // DFS (Depth First Search，深度優先搜尋)
    // 在樹(tree)或圖(graph)上找出從特定起點出發，抵達指定終點的最短距離(shortest path)
    // 可利用 stack 資料結構『先進後出』的特性，來確保「被搜尋到的位點，一個一個放入 stack 資料結構中，直到碰到無路可走，則從最後一個取出，再找尋是否有相鄰點，直到所有的 stack 被清出」。
    let [height, width] = lines[0].split(' ')

    // 迷宮大小
    height = Number(height)
    width = Number(width)

    // 此為迷宮，0 = 空格，1 = 障礙
    let maze = []
    for (let y = 0; y < height; y++) {
        let line = lines[y + 1]
        let tmp = []
        for (let x = 0; x < width; x++) {
            tmp[x] = (line[x] === '.') ? 0 : 1
        }
        maze[y] = tmp
    }

    // 設定起訖點
    let startx = 0
    let starty = 0
    let endx = width - 1
    let endy = height - 1

    // 定義一個表示走的方向的陣列 [x, y]
    let next = [
        [1, 0],   // 向右走
        [0, 1],   // 向下走
        [-1, 0],  // 向左走
        [0, -1]   // 向上走
    ]
    // run 100 times to check if the shortest route or not
    let stepResults = []
    for (let k = 0; k < 1000000; k++) {
        // 使用一個 Stack 資料結構
        let stack = new Stack()
        // 找到終點的旗標
        let flag = false

        let copyMaze = JSON.parse(JSON.stringify(maze))
        // 走過的路都會在maze地圖上的座標位置標成 2
        // 起點
        copyMaze[starty][startx] = 2

        // 將上一次的紀錄點存入 Stack 資料結構中
        stack.push([startx, starty])
        // console.log(copyMaze)

        while (stack.size() > 0) {
            // console.log(stack.toString())
            // 取出存在 Stack 最上層的紀錄點
            let point = stack.first()
            let findWay = false
            // 將4個方向順序打亂
            let randNext = next.sort((a, b) => 0.5 - Math.random())
            // 列舉4個方向
            for (let j = 0; j < randNext.length; j++) {
                // 計算下一個座標
                let nx = point[0] + randNext[j][0]
                let ny = point[1] + randNext[j][1]
                // console.log(nx, ny)
                // 判斷是否越界
                if (!(nx >= 0 && nx < width && ny >= 0 && ny < height)) continue
                // 判斷是否已經到達終點
                if (nx === endx && ny === endy) {
                    flag = true
                    break
                }
                // 判斷是否是障礙物或者已經走過
                if (copyMaze[ny][nx] === 0) {
                    copyMaze[ny][nx] = 2
                    // 存檔
                    stack.push([nx, ny])
                    // console.log(copyMaze)
                    findWay = true
                    break
                }
            }
            if (flag) break

            if (!findWay) {
                stack.pop()
            }
        }

        // 結果
        stepResults.push(stack.size())
    }
    console.log(stepResults)
    console.log(Math.min(...stepResults))
}

class Stack {
    constructor() {
        this.stack = []
    }
    push (val) {
        this.stack.push(val)
    }
    pop () {
        return this.stack.pop()
    }
    first () {
        return this.stack[this.stack.length - 1]
    }
    size () {
        return this.stack.length
    }
    toString () {
        return this.stack
    }
}
