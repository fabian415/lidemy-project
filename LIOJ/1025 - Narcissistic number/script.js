let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

function solve (lines) {
    let tmp = lines[0].split(' ')
    let n1 = Number(tmp[0])
    let n2 = Number(tmp[1])
    let result = ''
    for (let n = n1; n < (n2 + 1); n++) {
        if (n < 10) {
            console.log(n)
        } else {
            let str = n.toString()
            let len = str.length
            let total = 0
            for (let i = 0; i < len; i++) {
                total += Math.pow(Number(str[i]), len)
            }
            if (total === n) {
                console.log(n)
            }
        }
    }
    console.log(result)
}
