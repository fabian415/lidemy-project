let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// 題目：https://oj.lidemy.com/problem/1053
function solve (lines) {
    // BFS (Breadth First Search，廣度優先搜尋)
    // 在樹(tree)或圖(graph)上找出從特定起點出發，抵達指定終點的最短距離(shortest path)
    // 可利用 queue 資料結構『先進先出』的特性，來確保「先被搜尋到的頂點，會先成為下一個搜尋起點」。
    let [height, width] = lines[0].split(' ')

    // 迷宮大小
    height = Number(height)
    width = Number(width)

    // 此為迷宮，0 = 空格，1 = 障礙
    let maze = []
    for (let y = 0; y < height; y++) {
        let line = lines[y + 1]
        let tmp = []
        for (let x = 0; x < width; x++) {
            tmp[x] = (line[x] === '.') ? 0 : 1
        }
        maze[y] = tmp
    }
    console.log(maze)

    // 設定起訖點
    let startx = 0
    let starty = 0
    let endx = width - 1
    let endy = height - 1

    // 定義一個表示走的方向的陣列 [x, y]
    let next = [
        [1, 0],   // 向右走
        [0, 1],   // 向下走
        [-1, 0],  // 向左走
        [0, -1]   // 向上走
    ]

    // 使用一個 Queue 資料結構
    let queue = new Queue()
    // 步數
    let steps = 0
    // 找到終點的旗標
    let flag = false

    // 走過的路都會在maze地圖上的座標位置標成 2
    // 起點
    maze[starty][startx] = 2

    // 將上一次所有的紀錄點都存入 Queue 資料結構中
    let points = []
    points.push([startx, starty])
    queue.enqueue(points)
    // console.log(maze)

    while (queue.size() > 0) {
        // 取出存在 Queue 上的紀錄點
        let points = queue.dequeue()
        let nPoints = []
        // 列舉所有的紀錄點
        for (let i = 0; i < points.length; i++) {
            let point = points[i]
            // 列舉4個方向
            for (let j = 0; j < next.length; j++) {
                // 計算下一個座標
                let nx = point[0] + next[j][0]
                let ny = point[1] + next[j][1]
                // console.log(nx, ny)
                // 判斷是否越界
                if (!(nx >= 0 && nx < width && ny >= 0 && ny < height)) continue
                // 判斷是否是障礙物或者已經走過
                if (maze[ny][nx] === 0) {
                    maze[ny][nx] = 2
                    nPoints.push([nx, ny])
                }
                // 判斷是否已經到達終點
                if (nx === endx && ny === endy) {
                    flag = true
                    break
                }
            }
        }

        // 存檔
        if (nPoints.length > 0)
            queue.enqueue(nPoints)
        steps++
        if (flag) break
    }

    // 結果
    console.log(steps)
}

// 先進先出
class Queue {
    constructor() {
        this.queue = []
    }
    enqueue (val) {
        this.queue.unshift(val)
    }
    dequeue () {
        return this.queue.pop()
    }
    size () {
        return this.queue.length
    }
}
