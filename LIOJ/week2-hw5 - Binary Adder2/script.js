const readLine = require('readline')
const assert = require("assert")

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// 超級挑戰題
// 在這次的課程中有教到位元運算，你知道位元運算可以達到什麼目標嗎？讓我來告訴你！只用位元運算，就可以寫出相加兩個數字的功能。所以這一週的超級挑戰題就是：「挑戰寫出一個 function，可以接收兩個正整數，而且回傳相加的結果，並且在 function 裡面不能出現 +-*/ 任何一個符號」
// 參考資料：https://shengqian001.com/less/JavaScript%E5%8A%A0%E6%B3%95%E5%99%A8.html
// https://zhuanlan.zhihu.com/p/36630734
function solve (lines) {
    let result = add(Number(lines[0]), Number(lines[1]))
    console.log(result)
}

// 請實作以上函式，回傳 a+b 的結果
// 但是函式裡面不能出現 +-*/ 任何一個符號
function add (a, b) {
    while (b !== 0) {
        let sum = a ^ b // XOR 二進位加總
        b = (a & b) << 1 // AND 進位
        a = sum
    }
    return a
}

// a + b = (a ^ b) + (a & b) << 1
// a' = (a ^ b) ; b' = ( a&b )<<1
// a + b = a' + b'
//       = (a' ^ b') + (a' & b') << 1



