const readLine = require('readline')
const assert = require("assert")

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

// 超級挑戰題
// 在這次的課程中有教到位元運算，你知道位元運算可以達到什麼目標嗎？讓我來告訴你！只用位元運算，就可以寫出相加兩個數字的功能。所以這一週的超級挑戰題就是：「挑戰寫出一個 function，可以接收兩個正整數，而且回傳相加的結果，並且在 function 裡面不能出現 +-*/ 任何一個符號」
// 參考資料：https://shengqian001.com/less/JavaScript%E5%8A%A0%E6%B3%95%E5%99%A8.html
// https://zhuanlan.zhihu.com/p/36630734
function solve (lines) {
    let result = add(Number(lines[0]), Number(lines[1]))
    console.log(result)
}

// 請實作以上函式，回傳 a+b 的結果
// 但是函式裡面不能出現 +-*/ 任何一個符號
function add (a, b) {
    let maxBit = decideMaxBit(a, b)
    let bin_a = decimal2Binary(a, maxBit)
    let bin_b = decimal2Binary(b, maxBit)
    let bin_result = binaryAdder(bin_a, bin_b, maxBit)
    return binary2Decimal(bin_result.join(''))
}

/**
 * 半加器
 * 輸入: bit，輸出: array [carry, sum] 
 * carry: 進位
 * sum: 和
 * 
 * @param {bit} a 
 * @param {bit} b 
 */
function halfAdder (a, b) {
    return [a & b, a ^ b]
}

// 半加器測試
assert.deepStrictEqual(halfAdder(0, 0), [0, 0])
assert.deepStrictEqual(halfAdder(0, 1), [0, 1])
assert.deepStrictEqual(halfAdder(1, 0), [0, 1])
assert.deepStrictEqual(halfAdder(1, 1), [1, 0])

/**
 * 全加器
 * 輸入: bit，輸出: array [carry, sum] 
 * carry: 進位
 * sum: 和
 * 
 * @param {bit} a 
 * @param {bit} b 
 * @param {bit} c 
 */
function fullAdder (a, b, c) {
    let arr = halfAdder(a, b)
    let arr2 = halfAdder(arr[1], c)
    let carry = arr[0] | arr2[0]
    let sum = arr2[1]
    return [carry, sum]
}

// 全加器測試
assert.deepStrictEqual(fullAdder(0, 0, 0), [0, 0]);

assert.deepStrictEqual(fullAdder(1, 0, 0), [0, 1]);
assert.deepStrictEqual(fullAdder(0, 1, 0), [0, 1]);
assert.deepStrictEqual(fullAdder(0, 0, 1), [0, 1]);

assert.deepStrictEqual(fullAdder(1, 1, 0), [1, 0]);
assert.deepStrictEqual(fullAdder(1, 0, 1), [1, 0]);
assert.deepStrictEqual(fullAdder(0, 1, 1), [1, 0]);

assert.deepStrictEqual(fullAdder(1, 1, 1), [1, 1]);



/**
 * N位加法器
 * 如：
 * [0, 1, 0, 1],[0, 1, 0, 1] => [1, 0, 1, 0]
 * @param {Array<Number>} a  binary陣列，如：[0, 1, 0, 1]
 * @param {Array<Number>} b  binary陣列，如：[0, 1, 0, 1]
 * @param {Number} maxBit N位數
 * @returns {Array<Number>}
 */
function binaryAdder (a, b, maxBit) {
    let carry = 0
    let sum = 0
    let bit = maxBit - 1
    let result = []

    while (bit >= 0) {
        let temp = fullAdder(a[bit], b[bit], carry)
        carry = temp[0]
        sum = temp[1]
        result.push(sum)
        bit--
    }
    return result.reverse()
}


/**
 * 決定所需的位數
 * @param {Number, String} a 
 * @param {Number, String} b 
 * @returns 
 */
function decideMaxBit (a, b) {
    let n1 = new Number(a).toString(2).split('').length
    let n2 = new Number(b).toString(2).split('').length
    return Math.max(n1, n2) + 1
}

/**
 * 將十進位轉為二進位陣列
 * 如：
 * 1 => [0, 0, 0, 1]
 * 3 => [0, 0, 1, 1]
 * @param {Number, String} n 數字
 * @param {Number} maxBit 幾位數的二進位
 * @returns {Array<Number>}
 */
function decimal2Binary (n, maxBit) {
    return new Number(n).toString(2)
        .split('')
        .reverse()
        .concat(Array(maxBit).fill('0'))
        .slice(0, maxBit)
        .reverse()
        .map(i => +i)
}
// split: ["0", "1"]
// Array(4).fill("0"): ["0", "0", "0", "0"]
// concat: ["1", "0", "0", "0", "0", "0"]
// slice(0, 4): ["1", "0", "0", "0"]

/**
 * 將二進位轉為十進位
 * 如：
 * '1010' => 10
 * @param {String} str 二進位字串
 * @returns {Number}
 */
function binary2Decimal (str) {
    return parseInt(str, 2)
}

