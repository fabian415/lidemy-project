let readLine = require('readline')

let lines = []
let rl = readLine.createInterface({
    input: process.stdin
})

rl.on('line', function (line) {
    lines.push(line)
})

rl.on('close', function () {
    solve(lines)
})

function solve (lines) {
    let count = Number(lines[0])
    for (let i = 0; i < count; i++) {
        let n = Number(lines[1 + i])
        console.log(isPrime(n) ? 'Prime' : 'Composite')
    }
}

function isPrime (n) {
    if (n === 1) return false

    let limit = Math.floor(Math.sqrt(n))
    for (let i = 2; i <= limit; i++) {
        if (isInteger(n / i) && i !== n) return false
    }
    return true
}

function isInteger (numb) {
    return numb % 1 === 0
}
